package test2;

public class Question3_2 {

	public static void main(String[] args) {
		 String str1 = "Harmonize";
		 String str2 = "Harmo";
		 String str3 = (str2 + "nize");

		 boolean result = stringComparison(str1, str3);
		 System.out.println(result);
	}

	public static boolean stringComparison(String str1, String str3) {
		return str1.equals(str3);

	}

}
